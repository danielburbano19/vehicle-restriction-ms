const express = require('express')
const {DateTime} = require("luxon");
const app = express()
const port = 3000

let news = {
    "text": "new update...",
    "buttonLabel": "click here..",
    "url": "google.com",
    "isUpdate": false,
    "appVersion": "4"
}

news = {}

const restrictions = [
    {
        "date": "2022-07-11",
        "plates": "0-1"
    },
    {
        "date": "2022-07-12",
        "plates": "2-3"
    },
    {
        "date": "2022-07-13",
        "plates": "4-5"
    },
    {
        "date": "2022-07-14",
        "plates": "6-7"
    },
    {
        "date": "2022-07-15",
        "plates": "8-9"
    },
    {
        "date": "2022-07-16",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-07-17",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-07-18",
        "plates": "2-3"
    },
    {
        "date": "2022-07-19",
        "plates": "4-5"
    },
    {
        "date": "2022-07-20",
        "plates": "6-7"
    },
    {
        "date": "2022-07-21",
        "plates": "8-9"
    },
    {
        "date": "2022-07-22",
        "plates": "0-1"
    },
    {
        "date": "2022-07-23",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-07-24",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-07-25",
        "plates": "4-5"
    },
    {
        "date": "2022-07-26",
        "plates": "6-7"
    },
    {
        "date": "2022-07-27",
        "plates": "8-9"
    },
    {
        "date": "2022-07-28",
        "plates": "0-1"
    },
    {
        "date": "2022-07-29",
        "plates": "2-3"
    },
    {
        "date": "2022-07-30",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-07-31",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-01",
        "plates": "6-7"
    },
    {
        "date": "2022-08-02",
        "plates": "8-9"
    },
    {
        "date": "2022-08-03",
        "plates": "0-1"
    },
    {
        "date": "2022-08-04",
        "plates": "2-3"
    },
    {
        "date": "2022-08-05",
        "plates": "4-5"
    },
    {
        "date": "2022-08-06",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-07",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-08-08",
        "plates": "8-9"
    },
    {
        "date": "2022-08-09",
        "plates": "0-1"
    },
    {
        "date": "2022-08-10",
        "plates": "2-3"
    },
    {
        "date": "2022-08-11",
        "plates": "4-5"
    },
    {
        "date": "2022-08-12",
        "plates": "6-7"
    },
    {
        "date": "2022-08-13",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-14",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-15",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-08-16",
        "plates": "2-3"
    },
    {
        "date": "2022-08-17",
        "plates": "4-5"
    },
    {
        "date": "2022-08-18",
        "plates": "6-7"
    },
    {
        "date": "2022-08-19",
        "plates": "8-9"
    },
    {
        "date": "2022-08-20",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-21",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-22",
        "plates": "2-3"
    },
    {
        "date": "2022-08-23",
        "plates": "4-5"
    },
    {
        "date": "2022-08-24",
        "plates": "6-7"
    },
    {
        "date": "2022-08-25",
        "plates": "8-9"
    },
    {
        "date": "2022-08-26",
        "plates": "0-1"
    },
    {
        "date": "2022-08-27",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-28",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-08-29",
        "plates": "4-5"
    },
    {
        "date": "2022-08-30",
        "plates": "6-7"
    },
    {
        "date": "2022-08-31",
        "plates": "8-9"
    },
    {
        "date": "2022-09-01",
        "plates": "0-1"
    },
    {
        "date": "2022-09-02",
        "plates": "2-3"
    },
    {
        "date": "2022-09-03",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-04",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-05",
        "plates": "6-7"
    },
    {
        "date": "2022-09-06",
        "plates": "8-9"
    },
    {
        "date": "2022-09-07",
        "plates": "0-1"
    },
    {
        "date": "2022-09-08",
        "plates": "2-3"
    },
    {
        "date": "2022-09-09",
        "plates": "4-5"
    },
    {
        "date": "2022-09-10",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-11",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-12",
        "plates": "8-9"
    },
    {
        "date": "2022-09-13",
        "plates": "0-1"
    },
    {
        "date": "2022-09-14",
        "plates": "2-3"
    },
    {
        "date": "2022-09-15",
        "plates": "4-5"
    },
    {
        "date": "2022-09-16",
        "plates": "6-7"
    },
    {
        "date": "2022-09-17",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-18",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-19",
        "plates": "0-1"
    },
    {
        "date": "2022-09-20",
        "plates": "2-3"
    },
    {
        "date": "2022-09-21",
        "plates": "4-5"
    },
    {
        "date": "2022-09-22",
        "plates": "6-7"
    },
    {
        "date": "2022-09-23",
        "plates": "8-9"
    },
    {
        "date": "2022-09-24",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-25",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-09-26",
        "plates": "2-3"
    },
    {
        "date": "2022-09-27",
        "plates": "4-5"
    },
    {
        "date": "2022-09-28",
        "plates": "6-7"
    },
    {
        "date": "2022-09-29",
        "plates": "8-9"
    },
    {
        "date": "2022-09-30",
        "plates": "0-1"
    },
    {
        "date": "2022-10-01",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-02",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-03",
        "plates": "4-5"
    },
    {
        "date": "2022-10-04",
        "plates": "6-7"
    },
    {
        "date": "2022-10-05",
        "plates": "8-9"
    },
    {
        "date": "2022-10-06",
        "plates": "0-1"
    },
    {
        "date": "2022-10-07",
        "plates": "2-3"
    },
    {
        "date": "2022-10-08",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-09",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-10",
        "plates": "6-7"
    },
    {
        "date": "2022-10-11",
        "plates": "8-9"
    },
    {
        "date": "2022-10-12",
        "plates": "0-1"
    },
    {
        "date": "2022-10-13",
        "plates": "2-3"
    },
    {
        "date": "2022-10-14",
        "plates": "4-5"
    },
    {
        "date": "2022-10-15",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-16",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-17",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-10-18",
        "plates": "0-1"
    },
    {
        "date": "2022-10-19",
        "plates": "2-3"
    },
    {
        "date": "2022-10-20",
        "plates": "4-5"
    },
    {
        "date": "2022-10-21",
        "plates": "6-7"
    },
    {
        "date": "2022-10-22",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-23",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-24",
        "plates": "0-1"
    },
    {
        "date": "2022-10-25",
        "plates": "2-3"
    },
    {
        "date": "2022-10-26",
        "plates": "4-5"
    },
    {
        "date": "2022-10-27",
        "plates": "6-7"
    },
    {
        "date": "2022-10-28",
        "plates": "8-9"
    },
    {
        "date": "2022-10-29",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-30",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-10-31",
        "plates": "2-3"
    },
    {
        "date": "2022-11-01",
        "plates": "4-5"
    },
    {
        "date": "2022-11-02",
        "plates": "6-7"
    },
    {
        "date": "2022-11-03",
        "plates": "8-9"
    },
    {
        "date": "2022-11-04",
        "plates": "0-1"
    },
    {
        "date": "2022-11-05",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-06",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-07",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-11-08",
        "plates": "6-7"
    },
    {
        "date": "2022-11-09",
        "plates": "8-9"
    },
    {
        "date": "2022-11-10",
        "plates": "0-1"
    },
    {
        "date": "2022-11-11",
        "plates": "2-3"
    },
    {
        "date": "2022-11-12",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-13",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-14",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-11-15",
        "plates": "8-9"
    },
    {
        "date": "2022-11-16",
        "plates": "0-1"
    },
    {
        "date": "2022-11-17",
        "plates": "2-3"
    },
    {
        "date": "2022-11-18",
        "plates": "4-5"
    },
    {
        "date": "2022-11-19",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-20",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-21",
        "plates": "8-9"
    },
    {
        "date": "2022-11-22",
        "plates": "0-1"
    },
    {
        "date": "2022-11-23",
        "plates": "2-3"
    },
    {
        "date": "2022-11-24",
        "plates": "4-5"
    },
    {
        "date": "2022-11-25",
        "plates": "6-7"
    },
    {
        "date": "2022-11-26",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-27",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-11-28",
        "plates": "0-1"
    },
    {
        "date": "2022-11-29",
        "plates": "2-3"
    },
    {
        "date": "2022-11-30",
        "plates": "4-5"
    },
    {
        "date": "2022-12-01",
        "plates": "6-7"
    },
    {
        "date": "2022-12-02",
        "plates": "8-9"
    },
    {
        "date": "2022-12-03",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-04",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-05",
        "plates": "2-3"
    },
    {
        "date": "2022-12-06",
        "plates": "4-5"
    },
    {
        "date": "2022-12-07",
        "plates": "6-7"
    },
    {
        "date": "2022-12-08",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-12-09",
        "plates": "0-1"
    },
    {
        "date": "2022-12-10",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-11",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-12",
        "plates": "4-5"
    },
    {
        "date": "2022-12-13",
        "plates": "6-7"
    },
    {
        "date": "2022-12-14",
        "plates": "8-9"
    },
    {
        "date": "2022-12-15",
        "plates": "0-1"
    },
    {
        "date": "2022-12-16",
        "plates": "2-3"
    },
    {
        "date": "2022-12-17",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-18",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-19",
        "plates": "6-7"
    },
    {
        "date": "2022-12-20",
        "plates": "8-9"
    },
    {
        "date": "2022-12-21",
        "plates": "0-1"
    },
    {
        "date": "2022-12-22",
        "plates": "2-3"
    },
    {
        "date": "2022-12-23",
        "plates": "4-5"
    },
    {
        "date": "2022-12-24",
        "plates": "NA_WEEKENDS"
    },
    {
        "date": "2022-12-25",
        "plates": "NA_HOLIDAYS"
    },
    {
        "date": "2022-12-26",
        "plates": "8-9"
    },
    {
        "date": "2022-12-27",
        "plates": "0-1"
    },
    {
        "date": "2022-12-28",
        "plates": "2-3"
    },
    {
        "date": "2022-12-29",
        "plates": "4-5"
    },
    {
        "date": "2022-12-30",
        "plates": "6-7"
    },
    {
        "date": "2022-12-31",
        "plates": "NA_WEEKENDS"
    }
]


const basicAuth = require('express-basic-auth')

app.use(basicAuth({
    users: {'debs': 'debsVehicleRestrictionMs2022'}
}))

app.get('/restrictions', (req, res) => {
    const currentDate = DateTime.now().setZone('America/Bogota').toISODate();
    let indexCurrentDate = restrictions
        .map(restriction => DateTime.fromISO(restriction.date).toISODate())
        .indexOf(currentDate);
    res.send(restrictions.slice(indexCurrentDate, indexCurrentDate + 8))
})

app.get('/news', (req, res) => {
    res.send(news)
})

app.listen(port, () => {
    console.log(`Vehicle restriction ms listening on port ${port}`)
})
